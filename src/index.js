import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { unregister } from "./registerServiceWorker";
// import registerServiceWorker from './registerServiceWorker';

// if(window.location.hostname != "localhost") {
// document.domain = "ratewatch.com";
// //document.domain = "mortgagecoach.com";
// }

ReactDOM.render(<App />, document.getElementById("root"));
unregister();
// registerServiceWorker();
