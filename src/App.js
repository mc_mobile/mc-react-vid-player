import React, { Component } from "react";
import createReactClass from "create-react-class";
import logo from "./playlistAssets/MC_Logo_white.png";
import logo2 from "./playlistAssets/Mc_flower.png";
import "./App.css";
import "./styles.css";
import "./vid-styles.css";
import vidPlay from "./video-player.js";
import FullscreenIcon from "./FullscreenIcon.js";
import PlayIcon from "./PlayIcon.js";
import VolumeIcon from "./VolumeIcon.js";
import PauseIcon from "./PauseIcon.js";
import PlaylistIcon from "./PlaylistIcon";
import XIcon from "./XIcon.js";

// var videoPlayerSetupScript = require("video-player.js")
// var createReactClass = require("create-react-class");

var plurl = "https://s3-us-west-2.amazonaws.com/ratewatch-video/index.json";

var uagent = navigator.userAgent.toLowerCase();
function DetectIOS() {
  if (
    uagent.search("ipod") > -1 ||
    uagent.search("ipad") > -1 ||
    uagent.search("iphone") > -1
  )
    return true;
  else {
    return false;
  }
}

function DetectAndroid() {
  var ua = navigator.userAgent.toLowerCase();
  var isAndroid = ua.indexOf("android") > -1;
  return isAndroid;
}

// document.domain = "ratewatch.com"; <------ SET IN INDEX.JS

var extMediaTest = false;
var inEditorTest = false;

var App = createReactClass({
  createVideoJsPlayer: function (videoJsOptions) {
    if (this.player) {
      // this.player.dispose();
      console.log(this.state.vidSrc);
      this.player.src(
        this.state.vidSrc,
        this.state.vidSrc.indexOf(".mp4") > -1
          ? "video/mp4"
          : "application/x-mpegURL"
      );
    } else {
      console.log(videoJsOptions);
      this.player = window.videojs(
        this.video.current,
        videoJsOptions,
        function onPlayerReady() {
          // console.log("onPlayerReady", this)
        }
      );
    }
  },
  setUpVideo: function () {
    var hasExternalMediaSrc = false;
    var extMediaSrc = "";
    var extThumbSrc = "";

    if (extMediaTest === true) {
      extMediaSrc =
        "https://ae-video-content.s3-us-west-2.amazonaws.com/Mortgage+Coach+%26+SocialSurvey+Integration.mp4";
      extThumbSrc = "https://i3.ytimg.com/vi/U3JKvyYsx1k/hqdefault.jpg";
      hasExternalMediaSrc = true;
    }

    if (window.frameElement) {
      extMediaSrc = window.frameElement.getAttribute("data-mediasrc");
      extThumbSrc = window.frameElement.getAttribute("data-thumbsrc") || null;
      if (extMediaSrc && extMediaSrc.length > 0) {
        hasExternalMediaSrc = true;
      }
    }

    vidPlay();
    var supportsVideo = !!document.createElement("video").canPlayType;

    if (supportsVideo) {
      window.addResizeListener(this.figure.current, this.vidResized);

      this.video.current.addEventListener("timeupdate", this.timeUpdate);

      this.video.current.addEventListener(
        "loadedmetadata",
        function () {
          this.setState({
            duration: this.video.current.duration,
          });
        }.bind(this)
      );

      var inE = false; //inE means in editor
      console.log(document.referrer);
      if (window.parent !== window) {
        // if(document.referrer.indexOf(".mortgagecoach.com") > -1) {
        //   inE = true;
        //   console.log("inside the editor");
        // } else if(inEditorTest) {
        //   console.log("inside the editor test");
        //   inE = true;
        // }
        inE = true;
      }
      if (inEditorTest) {
        console.log("inside the editor test");
        inE = true;
      }

      console.log(inE);

      this.video.current.addEventListener(
        "ended",
        function () {
          if (this.state.hasPlaylist) {
            this.video.current.currentTime = 0;
            this.timeKnob.current.style["transform"] =
              "translateX(" + 0 + "px)";
            this.timeProgressSpan.current.style["width"] = 0 + "px";
            this.timeProgressBar.current.setAttribute("value", Math.round(0));

            if (
              this.state.selectedPlaylistItemIndex ==
              this.state.playlist.length - 1
            ) {
              this.plItemClick(0);
            } else {
              this.plItemClick(this.state.selectedPlaylistItemIndex + 1);
            }
            this.playPauseClick();
          } else {
            this.video.current.currentTime = 0;
            this.timeKnob.current.style["transform"] =
              "translateX(" + 0 + "px)";
            this.timeProgressSpan.current.style["width"] = 0 + "px";
            this.timeProgressBar.current.setAttribute("value", Math.round(0));
            this.playPauseClick();
          }
        }.bind(this)
      );

      if (this.state.playlistURL && !hasExternalMediaSrc) {
        this.getPlaylist(this.state.playlistURL, this.setPlaylist);
      } else if (hasExternalMediaSrc) {
        this.setState({
          setup: true,
          extMediaPlayer: true,
          vidSrc: extMediaSrc,
        });
        if (extThumbSrc) {
          this.setState({
            setup: true,
            vidPoster: extThumbSrc,
          });
        }
      } else {
        this.setState({
          setup: true,
        });
      }

      if (this.figure.current.offsetWidth <= 450) {
        this.setState({
          smallPlayer: true,
          smallPlaylist: true,
        });
      } else if (this.figure.current.offsetWidth <= 550) {
        this.setState({
          smallPlayer: true,
          smallPlaylist: false,
        });
      } else {
        this.setState({
          smallPlayer: false,
          smallPlaylist: false,
        });
      }
      window.setTimeout(
        function () {
          this.setState({
            showControls: false,
          });
        }.bind(this),
        3000
      );

      if (this.state.isMobile) {
        this.hideTimeout = null;
        this.figure.current.addEventListener(
          "touchstart",
          function () {
            if (this.hideTimeout) {
              window.clearTimeout(this.hideTimeout);
            }
            this.setState({
              showControls: true,
            });
          }.bind(this),
          false
        );
        this.figure.current.addEventListener(
          "touchend",
          function () {
            this.hideTimeout = window.setTimeout(
              function () {
                this.setState({
                  showControls: false,
                });
              }.bind(this),
              3000
            );
          }.bind(this),
          false
        );
      }

      var currTime = this.currTime.current;
      currTime.innerHTML = this.toHHMMSS(Math.floor(0));
    }
  },
  componentDidMount: function () {
    this.setUpVideo(); //remove useless code after setupvideo & refactor
  },
  componentDidUpdate: function (prevProps, prevState) {
    if (
      (this.state.vidSrc !== prevState.vidSrc && this.state.vidSrc) ||
      (this.state.vidPoster !== prevState.vidPoster && this.state.vidPoster)
    ) {
      var vidSrc = this.state.vidSrc;
      //vidSrc = "https://dw3qn9uoxiqr5.cloudfront.net/mcSocialSurveyhls.m3u8";
      var videoJsOptions = {
        autoplay: false,
        controls: true,
        sources: [
          {
            src: vidSrc,
            type:
              vidSrc.indexOf(".mp4") > -1
                ? "video/mp4"
                : "application/x-mpegURL",
            poster: this.state.vidPoster,
            html5: {
              hls: {
                withCredentials: true,
              },
            },
          },
        ],
      };
      this.createVideoJsPlayer(videoJsOptions);
    }
  },
  componentWillUnmount: function () {
    if (this.player) {
      this.player.dispose();
    }
  },
  vidResized: function () {
    if (this.figure.current.offsetWidth <= 450) {
      this.setState({
        smallPlayer: true,
        smallPlaylist: true,
      });
    } else if (this.figure.current.offsetWidth <= 550) {
      this.setState({
        smallPlayer: true,
        smallPlaylist: false,
      });
    } else {
      this.setState({
        smallPlayer: false,
        smallPlaylist: false,
      });
    }
    if (this.state.iosFS) {
      this.controls.current.style["bottom"] =
        "calc(50% - " +
        Math.ceil(this.video.current.offsetHeight / 2) +
        "px - 2.5rem";
    }
    this.timeUpdate();
  },
  getPlaylist: function (url, callback) {
    var xobj = new XMLHttpRequest();
    xobj.open("GET", url, true); // Replace "my_data" with the path to your file
    xobj.onreadystatechange = function () {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      if (xobj.readyState == 4 && xobj.status == "200") {
        if (callback) {
          callback(xobj.response);
        }
      }
    };
    xobj.send(null);
  },
  setPlaylist: function (playlistString) {
    var playlist = JSON.parse(playlistString);
    var bucketUrl = "https://s3-us-west-2.amazonaws.com/ratewatch-video/";
    var playlistArray = playlist.data;
    for (var i = 0; i < playlistArray.length; i++) {
      playlistArray[i]["content-type"] = "video/mp4";
      playlistArray[i]["url"] = bucketUrl + playlistArray[i].filename;
      // var thumbArr = playlistArray[i].filename.split(".");
      // thumbArr[thumbArr.length - 1] = "png";
      if (playlistArray[i]["author"] === "Douglas Wilken") {
        playlistArray[i]["thumbnail"] = bucketUrl + "ratewatch_Doug.jpeg";
      } else {
        playlistArray[i]["thumbnail"] = bucketUrl + "ratewatch_Dan.jpeg";
      }
      // playlistArray[i]["thumbnail"] = bucketUrl + thumbArr.join(".");
    }

    this.setState(
      {
        hasPlaylist: true,
        playlist: playlistArray,
        setup: true,
        vidSrc: playlistArray[0].url,
        vidPoster: playlistArray[0].thumbnail,
      },
      next.bind(this)
    );

    function next() {
      window.setTimeout(
        function () {
          this.video.current.load();
        }.bind(this),
        10
      );
    }
  },
  muteUnmuteVol: function () {
    var vid = this.video.current;
    var vol = vid.volume;
    var max = this.volumeProgressBar.current.offsetWidth;
    var lv = this.lastVol ? this.lastVol : 0;

    if (DetectIOS()) {
      if (vid.muted) {
        this.setVolumeRings(0.5);
        vid.muted = false;
      } else {
        this.setVolumeRings(0);
        vid.muted = true;
      }
      return;
    }

    if (vol == 0) {
      vid.volume = lv;
      var offset = max * lv;
      this.volumeKnob.current.style["transform"] =
        "translateX(" + offset + "px)";
      this.volumeProgressSpan.current.style["width"] = offset + "px";
      this.volumeProgressBar.current.setAttribute(
        "value",
        Math.round(lv * 100)
      );
      this.setVolumeRings(vid.volume);
    } else {
      vid.volume = 0;
      this.volumeKnob.current.style["transform"] = "translateX(" + 0 + "px)";
      this.volumeProgressSpan.current.style["width"] = 0 + "px";
      this.volumeProgressBar.current.setAttribute("value", Math.round(0 * 100));
      this.setVolumeRings(0, true);
    }
  },
  setVolumeRings: function (vol, dontSetLastVol) {
    var vid = this.video.current;
    vid.muted = false;
    if (vol == 0) {
      vid.muted = true;
      this.setState({
        volRings: "zero",
      });
    } else if (vol > 0 && vol < 0.3) {
      this.setState({
        volRings: "none",
      });
    } else if (vol >= 0.3 && vol < 0.65) {
      this.setState({
        volRings: "one",
      });
    } else {
      this.setState({
        volRings: "two",
      });
    }
    if (!dontSetLastVol) {
      this.lastVol = vol;
    }
  },
  clickMoveVolume: function (evt) {
    var vpb = this.volumeProgressBar.current;
    var vpr = this.volumeProgressDiv.current;

    var offset = vpb.offsetLeft;
    var offsetNode = vpb.offsetParent;
    offset += offsetNode.offsetLeft;
    offsetNode = offsetNode.offsetParent;
    while (offsetNode) {
      offset += offsetNode.offsetTop;
      offsetNode = offsetNode.offsetParent;
    }

    offset = offset - evt.pageY - vpr.offsetWidth - vpr.offsetLeft;

    var max = this.volumeProgressBar.current.offsetWidth;
    this.video.current.volume = offset / max;
    this.volumeKnob.current.style["transform"] = "translateX(" + offset + "px)";
    this.volumeProgressSpan.current.style["width"] = offset + "px";
    this.volumeProgressBar.current.setAttribute(
      "value",
      Math.round((offset / max) * 100)
    );
    this.setVolumeRings(this.video.current.volume);
    this.moveVolumeKnob(evt);
  },
  moveVolumeKnob: function (evt) {
    var regex = /\d+/g;
    var initOffset = this.volumeKnob.current.style["transform"].match(regex);
    if (initOffset) {
      initOffset = parseInt(initOffset[0]);
    } else {
      initOffset = 0;
    }

    this.setState({
      draggingVolume: true,
      initVolY: evt.pageY,
      initVolOffset: initOffset,
    });
    document
      .querySelector("body")
      .addEventListener("mousemove", this.dragVolume);
    document
      .querySelector("body")
      .addEventListener("mouseup", this.stopVolumeDrag);
  },
  dragVolume: function (evt) {
    if (!this.state.draggingVolume) {
      return;
    }
    var max = this.volumeProgressBar.current.offsetWidth;
    var offset = this.state.initVolOffset + this.state.initVolY - evt.pageY;
    if (offset > max) {
      offset = max;
    } else if (offset < 0) {
      offset = 0;
    }
    this.video.current.volume = offset / max;
    this.volumeKnob.current.style["transform"] = "translateX(" + offset + "px)";
    this.volumeProgressSpan.current.style["width"] = offset + "px";
    this.volumeProgressBar.current.setAttribute(
      "value",
      Math.round((offset / max) * 100)
    );
    this.setVolumeRings(this.video.current.volume);
  },
  stopVolumeDrag: function (evt) {
    this.setState({
      draggingVolume: false,
    });
    document
      .querySelector("body")
      .removeEventListener("mousemove", this.dragVolume);
    document
      .querySelector("body")
      .removeEventListener("mouseup", this.stopVolumeDrag);
  },
  timeProgressClick: function (evt) {
    var offset = this.timeProgressBar.current.offsetLeft;
    var offsetNode = this.timeProgressBar.current.offsetParent;
    var regex = /\d+/g;
    while (offsetNode) {
      offset += offsetNode.offsetLeft;
      offsetNode = offsetNode.offsetParent;
    }

    var pos = (evt.pageX - offset) / this.timeProgressBar.current.offsetWidth;

    this.video.current.currentTime = pos * this.video.current.duration;
    this.timeKnob.current.style["transform"] =
      "translateX(" + pos * this.timeProgressBar.current.offsetWidth + "px)";
    this.timeProgressSpan.current.style["width"] =
      pos * this.timeProgressBar.current.offsetWidth + "px";
    this.timeProgressBar.current.setAttribute(
      "value",
      Math.round(this.video.current.currentTime)
    );
    this.moveTimeKnob(evt);
  },
  moveTimeKnob: function (evt) {
    var regex = /\d+/g;
    var initOffset = this.timeKnob.current.style["transform"].match(regex);
    if (initOffset) {
      initOffset = parseInt(initOffset[0]);
    } else {
      initOffset = 0;
    }
    this.setState({
      draggingTime: true,
      initTimeX: evt.pageX,
      initTimeOffset: initOffset,
    });
    document.querySelector("body").addEventListener("mousemove", this.dragTime);
    document
      .querySelector("body")
      .addEventListener("mouseup", this.stopTimeDrag);
  },
  dragTime: function (evt) {
    if (!this.state.draggingTime) {
      return;
    }
    var vid = this.video.current;
    var max = this.timeProgressBar.current.offsetWidth;
    var offset = this.state.initTimeOffset + evt.pageX - this.state.initTimeX;
    if (offset > max) {
      offset = max;
    } else if (offset < 0) {
      offset = 0;
    }
    vid.currentTime = (offset / max) * vid.duration;
    this.timeKnob.current.style["transform"] = "translateX(" + offset + "px)";
    this.timeProgressSpan.current.style["width"] = offset + "px";
    this.timeProgressBar.current.setAttribute("value", vid.currentTime);
  },
  stopTimeDrag: function (evt) {
    this.setState({
      draggingTime: false,
    });
    document
      .querySelector("body")
      .removeEventListener("mousemove", this.dragTime);
    document
      .querySelector("body")
      .removeEventListener("mouseup", this.stopTimeDrag);
  },
  playPauseClick: function () {
    var video = this.video.current;

    if (!this.state.beenPlayed) {
      video.volume = 0.1;
      var max = this.volumeProgressBar.current.offsetWidth;
      var offset = max * 0.1;
      this.volumeKnob.current.style["transform"] =
        "translateX(" + offset + "px)";
      this.volumeProgressSpan.current.style["width"] = offset + "px";
      this.volumeProgressBar.current.setAttribute(
        "value",
        Math.round((offset / max) * 100)
      );
      this.setVolumeRings(DetectIOS() ? 0.5 : video.volume);
    }
    if ((video.paused || video.ended) && video.readyState >= 2) {
      this.setState(
        {
          paused: false,
          beenPlayed: true,
        },
        function () {
          video.play();
        }
      );
    } else {
      this.setState(
        {
          paused: true,
        },
        function () {
          video.pause();
        }
      );
    }
  },
  timeUpdate: function () {
    var tpb = this.timeProgressBar.current;
    var vid = this.video.current;

    if (!tpb.getAttribute("max")) {
      tpb.setAttribute("max", vid.duration);
    }

    var max = tpb.offsetWidth;
    tpb.value = vid.currentTime;
    var width = (vid.currentTime / vid.duration) * max;
    this.timeProgressSpan.current.style["width"] = width + "px";
    this.timeKnob.current.style["transform"] = "translateX(" + width + "px)";

    var currTime = this.currTime.current;
    currTime.innerHTML = this.toHHMMSS(Math.floor(vid.currentTime));
  },
  toHHMMSS: function (secs) {
    if (isNaN(secs)) {
      secs = 0;
    }
    secs = Math.floor(secs);
    var sec_num = secs; // don"t forget the second dparam
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - hours * 3600) / 60);
    var seconds = sec_num - hours * 3600 - minutes * 60;

    if (hours == 0) {
      hours = "";
    } else if (hours < 10) {
      hours = "0" + hours + ":";
    } else {
      hours = hours + ":";
    }

    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }

    return hours + minutes + ":" + seconds;
  },
  stopClickPropagation: function (evt) {
    evt.stopPropagation();
  },
  buildPlaylist: function () {
    return (
      <div
        id="playlistContainer"
        onClick={this.stopClickPropagation}
        className={this.state.smallPlaylist ? "small" : ""}
      >
        <div
          id="playlist-tab"
          onClick={this.showPlaylist}
          className={this.state.playlistShown ? "hidden" : ""}
        >
          {PlaylistIcon("playlist-icon")}
        </div>
        <div id="playlist" className={this.state.playlistShown ? "shown" : ""}>
          <div id="playlist-title">More Videos</div>
          {XIcon("playlist-x", "", { onClick: this.hidePlaylist })}
          {this.buildPlaylistBody()}
        </div>
      </div>
    );
  },
  buildPlaylistBody: function () {
    var plItems = [];
    var plData = this.state.playlist;

    for (var i = 0; i < plData.length; i++) {
      plItems.push(
        <div
          key={i.toString()}
          onClick={this.plItemClick.bind(this, i)}
          className={
            "playlist-item" +
            (i == this.state.selectedPlaylistItemIndex ? " selected" : "")
          }
        >
          <div className="item-left">
            <div className="item-title">{plData[i].title}</div>
            {/* <div className="item-description">{plData[i].description}</div> */}
            <div className="item-author" style={{ fontSize: "1.125rem" }}>
              {"By: " + plData[i].author}
            </div>
          </div>
          <img className="item-thumb" src={plData[i].thumbnail} />
        </div>
      );
    }

    return <div id="playlist-body">{plItems}</div>;
  },
  plItemClick: function (args) {
    // var vid = this.video.current;
    var _this = this;

    this.video.current.currentTime = 0;
    this.timeKnob.current.style["transform"] = "translateX(" + 0 + "px)";
    this.timeProgressSpan.current.style["width"] = 0 + "px";
    this.timeProgressBar.current.setAttribute("value", Math.round(0));

    var currTime = this.currTime.current;
    currTime.innerHTML = this.toHHMMSS(Math.floor(0));

    this.setState(
      {
        selectedPlaylistItemIndex: args,
        vidSrc: this.state.playlist[args].url,
        vidPoster: this.state.playlist[args].thumbnail,
      },
      next
    );

    function next() {
      window.setTimeout(
        function () {
          _this.video.current.load();
          _this.hidePlaylist();
        }.bind(this),
        200
      );
    }
  },
  showPlaylist: function () {
    var x = document.querySelectorAll(".playlist-item")[
      this.state.selectedPlaylistItemIndex
    ];
    document.querySelector("#playlist-body").scrollTop = x.offsetTop - 57;
    this.setState(
      {
        playlistShown: true,
      },
      next.bind(this)
    );
    function next() {
      if (!this.state.paused) {
        this.playPauseClick();
      }
    }
  },
  hidePlaylist: function () {
    this.setState(
      {
        playlistShown: false,
      },
      next.bind(this)
    );
    function next() {
      if (this.state.paused) {
        this.playPauseClick();
      }
    }
  },
  handleFullscreen: function () {
    if (DetectIOS()) {
      if (!this.state.iosFS) {
        this.controls.current.style["bottom"] =
          "calc(50% - " +
          Math.ceil(this.video.current.offsetHeight / 2) +
          "px - 2.5rem";
      } else {
        this.controls.current.removeAttribute("style");
      }
      this.setState({
        iosFS: !this.state.iosFS,
      });
    } else {
      if (this.state.setup) {
        vidPlay.handleFullscreen();
        this.setState({
          isFullscreen: !this.state.isFullscreen,
        });
      }
    }
  },
  handleFullScreenControls: function () {
    var _this = this;
    if (!this.state.isFullscreen) {
      return;
    }
    if (this.hideTimeoutFS) {
      window.clearTimeout(this.hideTimeoutFS);
    }
    if (!this.state.showControls) {
      this.setState(
        {
          showControls: true,
        },
        next.bind(this)
      );
    } else {
      next();
    }

    function next() {
      _this.hideTimeoutFS = window.setTimeout(function () {
        _this.setState({
          showControls: false,
        });
      }, 3000);
    }
  },
  render: function () {
    var stayOpen = this.state.draggingVolume || this.state.draggingTime;
    var volumeStayOpen = this.state.draggingVolume;

    var fClasses = [];
    if (stayOpen || this.state.showControls) {
      fClasses.push("stay-open");
    }
    if (this.state.playlistShown) {
      fClasses.push("showing-playlist");
    }
    if (this.state.smallPlayer) {
      fClasses.push("small-player");
    }
    if (this.state.isFullscreen) {
      fClasses.push("fullscreen");
    }
    if (this.state.isIOS) {
      fClasses.push("is-ios");
    }
    if (this.state.iosFS) {
      fClasses.push("ios-fullscreen");
    }
    if (this.state.isMobile) {
      fClasses.push("is-mobile");
    }
    return (
      <div className="App">
        <figure
          id="videoContainer"
          data-fullscreen="false"
          className={fClasses.join(" ")}
          ref={this.figure}
          onMouseMove={this.handleFullScreenControls}
        >
          {this.state.beenPlayed ? null : (
            <img
              style={{ display: "none" }}
              onClick={this.playPauseClick}
              src={this.state.vidPoster}
              className="video-thumbnail-img"
            />
          )}
          <div data-vjs-player>
            <video
              onClick={this.playPauseClick}
              poster={this.state.vidPoster}
              crossOrigin="anonymous"
              className={this.state.extMediaPlayer ? "ext-media-player" : ""}
              playsInline="true"
              webkit-playsinline="true"
              id="video"
              preload="metadata"
              ref={this.video}
            ></video>
          </div>
          <div
            id="playlist-background"
            className={this.state.playlistShown ? "shown" : ""}
          ></div>
          {this.state.hasPlaylist ? this.buildPlaylist() : null}
          <div
            id="video-controls"
            onClick={this.stopClickPropagation}
            className="controls"
            data-state="hidden"
            ref={this.controls}
          >
            <div
              id="time-progress"
              className="progress"
              ref={this.timeProgressDiv}
            >
              <span
                id="time-knob"
                onMouseDown={this.moveTimeKnob}
                ref={this.timeKnob}
              ></span>
              <progress
                id="progress"
                value="0"
                min="0"
                max={this.state.duration}
                ref={this.timeProgressBar}
                onMouseDown={this.timeProgressClick}
              >
                <span id="progress-bar" ref={this.timeProgressSpan}></span>
              </progress>
            </div>
            <div className="buttons-container">
              <div className="mc-logo-container">
                <img
                  className={this.state.smallPlayer ? "small" : ""}
                  src={this.state.smallPlayer ? logo2 : logo}
                />
              </div>
              <div id="time-play-pause-container">
                <div
                  id="at-time"
                  className="time start"
                  ref={this.currTime}
                ></div>
                <div onClick={this.playPauseClick} id="play-pause-icon">
                  {this.state.paused || this.state.beenPlayed === false
                    ? PlayIcon("play-icon")
                    : PauseIcon("pause-icon")}
                </div>
                <div id="total-time" className="time end">
                  {this.toHHMMSS(this.state.duration)}
                </div>
              </div>
              <div className="volume-fullscreen-container">
                <div
                  id="volume-icon-container"
                  className={volumeStayOpen ? "stay-open" : ""}
                >
                  <div id="volume-progress" ref={this.volumeProgressDiv}>
                    <span
                      id="volume-knob"
                      onMouseDown={this.moveVolumeKnob}
                      ref={this.volumeKnob}
                    ></span>
                    <progress
                      onMouseDown={this.clickMoveVolume}
                      value="0"
                      min="0"
                      max="100"
                      ref={this.volumeProgressBar}
                    >
                      <span
                        id="volume-progress-bar"
                        ref={this.volumeProgressSpan}
                      ></span>
                    </progress>
                    <div id="vol-bot-arrow"></div>
                  </div>
                  {VolumeIcon("vol-icon", this.state.volRings, {
                    onClick: this.muteUnmuteVol,
                  })}
                </div>
                <div
                  id="fullscreen-icon-container"
                  onClick={this.handleFullscreen}
                >
                  {FullscreenIcon()}
                </div>
              </div>
            </div>
          </div>
        </figure>
      </div>
    );
  },
  getInitialState: function () {
    var state = {};

    state.setup = false;
    state.paused = true;
    state.beenPlayed = false;
    state.hasPlaylist = false;
    state.playlistShown = false;
    state.playlistURL = plurl;
    state.playlist = [];
    state.selectedPlaylistItemIndex = 0;
    state.vidSrc = "";
    state.vidPoster = "";
    state.extMediaPlayer = false;

    state.draggingTime = false;
    state.initTimeX = null;
    state.initTimeOffset = 0;
    state.currentTime = 0;
    state.duration = 0;

    state.draggingVolume = false;
    state.initVolY = null;
    state.initVolOffset = 0;
    state.volRings = "none";

    state.smallPlayer = false;
    state.showControls = true;

    state.isMobile = DetectAndroid() || DetectIOS();
    state.isIOS = DetectIOS();
    state.iosFS = false;
    state.isFullscreen = false;

    this.controls = React.createRef();

    this.timeKnob = React.createRef();
    this.timeProgressBar = React.createRef();
    this.timeProgressSpan = React.createRef();
    this.timeProgressDiv = React.createRef();
    this.currTime = React.createRef();

    this.volumeKnob = React.createRef();
    this.volumeProgressBar = React.createRef();
    this.volumeProgressSpan = React.createRef();
    this.volumeProgressDiv = React.createRef();

    this.video = React.createRef();

    this.figure = React.createRef();

    return state;
  },
});

export default App;
