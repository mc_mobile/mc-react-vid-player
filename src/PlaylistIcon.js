import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

function PlaylistIcon(id, classN, evts) {
    var ID = id ? id : "";
    var cln = classN ? classN : "" 
    var icon =  <svg id={ID} className={cln} version="1.1" onClick={evts ? evts.onClick : null}>
                    <g>
                        <circle fill="#ffffff" stroke="#ffffff" id="svg_1" r="2" cy="12" cx="12"/>
                        <circle fill="#ffffff" stroke="#ffffff" id="svg_2" r="2" cy="5" cx="12"/>
                        <circle fill="#ffffff" stroke="#ffffff" id="svg_3" r="2" cy="19" cx="12"/>
                    </g>
                </svg>;

    return icon;
}

export default PlaylistIcon;