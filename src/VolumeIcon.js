import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

function VolumeIcon(id, classN, evts) {
    var ID = id ? id : "";
    var cln = classN ? classN : "" 
    var icon =  <svg id={ID} className={cln} version="1.1" viewBox="0 0 512 512" onClick={evts ? evts.onClick : null}>
                    <g>
                        <polygon points="270,407.7 270,104.4 175.3,192 71,192 71,320 175.3,320"/>
                        <polygon points="511.096,161.373 476.855,127.132 382.68,221.307 288.506,127.132 254.265,161.373 348.44,255.548     254.265,349.723 288.506,383.964 382.68,289.789 476.855,383.964 511.096,349.723 416.921,255.548   " className="vol-x-icon"></polygon>
                        <path d="M326.3,355.6c20.5-27.8,32.8-62.3,32.8-99.6c0-37.4-12.3-71.8-32.8-99.6l-20.4,15.3c17.4,23.6,27.8,52.7,27.8,84.3   c0,31.6-10.4,60.7-27.8,84.3L326.3,355.6z"/>
                        <path d="M392.8,401.6c30-40.7,48-91,48-145.6s-18-104.9-48-145.6l-20.4,15.3c26.9,36.4,43,81.4,43,130.3c0,48.9-16.1,93.8-43,130.3   L392.8,401.6z"/>
                    </g>
                </svg>;

    return icon;
}

export default VolumeIcon;