import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

function PauseIcon(id, classN) {
    var ID = id ? id : "";
    var cln = classN ? classN : "" 
    var icon =  <svg id={ID} className={cln} version="1.1" viewBox="0 0 23 29">
                    <g>
                        <rect fill="white" height="100%" width="33%"/>
                        <rect fill="white" height="100%" width="33%" x="66%"/>
                    </g>
                </svg>;

    return icon;
}

export default PauseIcon;