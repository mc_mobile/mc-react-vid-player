import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

function PlayIcon(id, classN) {
    var ID = id ? id : "";
    var cln = classN ? classN : "" 
    var icon =  <svg id={ID} className={cln} version="1.1" viewBox="0 0 512 512">
                    <g>
                        <path d="M128,96v320l256-160L128,96L128,96z"/>
                    </g>
                </svg>;

    return icon;
}

export default PlayIcon;